# -*- coding: utf-8 -*-
# vim: ft=jinja

{% from "linux_ad/map.jinja" import "linux_ad_setting" as ad with context %}

{% set test_join_cmd = "realm discover " ~ ad.realm | upper ~ " | grep -qs 'configured: kerberos-member'" %}
{% set test_ad_already_connect = "realm discover" | grep -qs 'configured: kerberos-member' %}

{% for packages in ad.pkgs %}
install_dependencies:
  pkg.installed: {{ packages }}
{% endfor %}

add_password_file:
  file.managed:
    - name: {{ ad.password.file_path }}
    - contents: {{ ad.password.password }}
    - contents_newline: False
    - unless: {{ test_ad_already_connect }}
    - required: 
      - pkg: install_dependencies

store_password_in_knit:
  cmd.run:
    - name: knit --pasword-file={{ ad.password.file_path }} {{ ad.username }}
    - onchanges:
      - file: add_password_file

join_ad:
  cmd.run:
    - name: realm -v join   "{{ ad.host }}"
    - onchanges:
      - cmd: store_password_in_knit

destroy_knit_store_password:
  cmd.run:
    - name: kdestroy
    - onchanges:
      - cmd: join_ad

remove_password_file:
  file.absent:
    - name: {{ ad.password.file_path }}
