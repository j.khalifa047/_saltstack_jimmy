# -*- coding: utf-8 -*-
# vim: ft=jinja

{% from "windows_join_domain/map.jinja" import "windows_ad_setting" as ad with context  %}

ad_member:
  system.join_domain:
    - name: {{ ad.host }}
    - username: {{ ad.username }}
    - password: {{ ad.password }}
    - restart: False

restart_system_to_reflect+changes:
  system.reboot:
    - message: Your system is Joining with Active Directory {{ ad.host }}
    - timeout: 10
    - in_seconds: True
    - only_on_pending_reboot: True
    - onchanges:
      - system: ad_member